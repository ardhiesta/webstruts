<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/struts-tags" prefix="s"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Login Page</title>
    </head>
    <body>
        <s:form action="login">
            <s:textfield name="name" label="User ID"></s:textfield>
            <s:textfield name="password" label="Password" type="password"></s:textfield>
            <s:submit value="Login"></s:submit>
	</s:form>
    </body>
</html>