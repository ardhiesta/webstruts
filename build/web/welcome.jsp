<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<jsp:include page="loginCheck.jsp" />
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Welcome</title>
        <script>
            function confirmBox() {
                var answer;
                answer = window.confirm("Do you really want to perform this action");
                if (answer == true) {
                    return true;
                } else {
                    return false;
                }
            }
        </script>
    </head>
 
    <body>
        <h2>Struts 2 - Login Application</h2>
        <s:if test="#session.logined != 'true'">
            <jsp:forward page="login.jsp" />
        </s:if>
        <s:else>
            <s:if test="mhsList.size() > 0">
                <div class="content">
                    <table class="mhsTable" cellpadding="5px">
                        <tr class="even">
                            <th>ID</th>
                            <th>NAMA</th>
                        </tr>
                        <s:iterator value="mhsList">
                            <tr>
                                <td><s:property value="ID" /></td>
                                <td><s:property value="NAMA" /></td>
                                <td>
                                    <s:url id="editURL" action="editMhs">
                                        <s:param name="ID" value="%{ID}"></s:param>
                                    </s:url>
                                    <s:a href="%{editURL}">Edit</s:a>
                                </td>
                                <td>
                                    <s:url id="deleteURL" action="deleteMahasiswa">
                                        <s:param name="ID" value="%{ID}"></s:param>
                                    </s:url>
                                    <s:a href="%{deleteURL}" onclick = "return confirmBox();">Delete</s:a>
                                </td>
                            </tr>
                        </s:iterator>
                    </table>
                </div>
            </s:if>
            You have been logged in successfully. <a href="./logout.action">logout</a>
        </s:else>
    </body>
</html>