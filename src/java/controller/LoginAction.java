/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.Date;
import java.util.Map;
import utility.DbQuery;

/**
 *
 * @author linuxluv
 */
public class LoginAction extends ActionSupport {
    private String name;
    private String password;
    DbQuery dq = new DbQuery();
    
    @Override
    public void validate(){
        if(name.length()==(0))
            this.addFieldError("name", "Name is required");
        if(password.length()==(0))
            this.addFieldError("password", "Password is required");
    }
    @Override
    public String execute() throws Exception {
        if(dq.find(getName(),getPassword())){
        	Map session = ActionContext.getContext().getSession();
        	session.put("logined","true");
        	session.put("context", new Date());
        	            
            return SUCCESS;
        } else
            this.addActionError("Invalid username and password");
            return ERROR;
    }
    
    public String logout() throws Exception {
//      HttpSession session = ServletActionContext.getRequest().getSession();
//      session.removeAttribute("logined");
//      session.removeAttribute("context");
    	Map session = ActionContext.getContext().getSession();
    	session.remove("logined");
    	session.remove("context");
    	return SUCCESS;
    }
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
}
