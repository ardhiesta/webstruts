/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.Map;

/**
 *
 * @author linuxluv
 */
public class LogoutAction extends ActionSupport {
    public String execute() throws Exception {
    	Map session = ActionContext.getContext().getSession();
    	session.remove("logined");
    	session.remove("context");
        return SUCCESS;
    }
}
