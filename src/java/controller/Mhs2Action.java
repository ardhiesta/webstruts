/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import model.Mahasiswa2;
import org.apache.struts2.ServletActionContext;
import utility.DbQuery;

/**
 *
 * @author linuxluv
 */
public class Mhs2Action extends ActionSupport implements ModelDriven<Mahasiswa2> {
    private Mahasiswa2 mahasiswa2 = new Mahasiswa2();
    private List<Mahasiswa2> mhsList2 = new ArrayList<Mahasiswa2>();
    private DbQuery dq = new DbQuery();

    public Mahasiswa2 getMahasiswa2() {
        return mahasiswa2;
    }

    public void setMahasiswa2(Mahasiswa2 mahasiswa2) {
        this.mahasiswa2 = mahasiswa2;
    }

    public List<Mahasiswa2> getMhsList2() {
        return mhsList2;
    }

    public void setMhsList2(List<Mahasiswa2> mhsList2) {
        this.mhsList2 = mhsList2;
    }

    @Override
    public Mahasiswa2 getModel() {
        return mahasiswa2;
    }
    
    public String list(){
        mhsList2 = dq.listMahasiswa2();
        return SUCCESS;
    }
    
    public String edit(){
        HttpServletRequest request = (HttpServletRequest) ActionContext.getContext().get( ServletActionContext.HTTP_REQUEST);
        mahasiswa2 = dq.getMhsById2(request.getParameter("ID"));
        return SUCCESS;
    }
    
    public String update(){
        HttpServletRequest request = (HttpServletRequest) ActionContext.getContext().get( ServletActionContext.HTTP_REQUEST);
        dq.updateMahasiswa2(request.getParameter("ID"), request.getParameter("NIM"), request.getParameter("NAMA"));
        return SUCCESS;
    }
    
    //untuk insert record baru
    public String insert(){
        HttpServletRequest request = (HttpServletRequest) ActionContext.getContext().get( ServletActionContext.HTTP_REQUEST);
//        dq.updateMahasiswa2(request.getParameter("ID"), request.getParameter("NIM"), request.getParameter("NAMA"));
        dq.insertMahasiswa2(request.getParameter("NIM"), request.getParameter("NAMA"));
        return SUCCESS;
    }
}
