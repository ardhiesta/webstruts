/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

//import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import model.Mahasiswa;
import org.apache.struts2.ServletActionContext;
import utility.DbQuery;

/**
 *
 * @author linuxluv
 */
public class MhsAction extends ActionSupport implements ModelDriven<Mahasiswa> {
    private Mahasiswa mahasiswa = new Mahasiswa();
    private List<Mahasiswa> mhsList = new ArrayList<Mahasiswa>();
    private DbQuery dq = new DbQuery();
    private String prev_mhsId;
//    private String prev_mhsId;
//    private String prev_mhsId;

    public String getPrev_mhsId() {
        return prev_mhsId;
    }

    public void setPrev_mhsId(String prev_mhsId) {
        this.prev_mhsId = prev_mhsId;
    }
    
    @Override
    public Mahasiswa getModel() {
        return mahasiswa;
    }
    
    public String list(){
        mhsList = dq.listMahasiswa();
        return SUCCESS;
    }
    
//    @Override
//    public String input() throws Exception {
//        setMahasiswa(mahasiswa);
//        return INPUT;
//    }
    
    public String update(){
//        System.out.println(">> cek ID di action class: "+getPrev_mhsId()+" | "+mahasiswa.getID()+" | "+mahasiswa.getNAMA());
//        String[] idSplit = mahasiswa.getID().split(", ");
//        System.out.println("id lama: "+idSplit[0]);
        
//        dq.updateMahasiswa(mahasiswa);
        HttpServletRequest request = (HttpServletRequest) ActionContext.getContext().get( ServletActionContext.HTTP_REQUEST);
//        System.out.println(">> prevId : "+request.getParameter("prev_mhsId"));
//        mahasiswa = dq.getMhsById(request.getParameter("prev_mhsId"));
//        System.out.println(">> prevMhs : "+mahasiswa.getID());
        dq.updateMahasiswa(request.getParameter("prev_mhsId"), request.getParameter("ID"), request.getParameter("NAMA"));
        return SUCCESS;
    }    
    
    public String edit(){
        HttpServletRequest request = (HttpServletRequest) ActionContext.getContext().get( ServletActionContext.HTTP_REQUEST);
        mahasiswa = dq.getMhsById(request.getParameter("ID"));
        return SUCCESS;
    }
    
    public String delete(){
        HttpServletRequest request = (HttpServletRequest) ActionContext.getContext().get( ServletActionContext.HTTP_REQUEST);
//        userDAO.deleteUser(Long.parseLong( request.getParameter("id")));
        System.out.println(">> ID : "+request.getParameter("ID"));
        dq.deleteMahasiswa(request.getParameter("ID"));
        return SUCCESS;
    }
    
//    @Override
//    public String execute() throws Exception {
//        mhsList = dq.listMahasiswa();
//        return "success";
//    }

    public Mahasiswa getMahasiswa() {
        return mahasiswa;
    }

    public void setMahasiswa(Mahasiswa mahasiswa) {
        this.mahasiswa = mahasiswa;
    }

    public List<Mahasiswa> getMhsList() {
        return mhsList;
    }

    public void setMhsList(List<Mahasiswa> mhsList) {
        this.mhsList = mhsList;
    }
}
