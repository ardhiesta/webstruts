/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utility;

import java.util.Iterator;
import java.util.List;
import model.Mahasiswa;
import model.Mahasiswa2;
import model.User;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author linuxluv
 */
public class DbQuery {
    public void insertMahasiswa2(String NIM, String NAMA){
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        Mahasiswa2 mahasiswa2 = new Mahasiswa2();
        mahasiswa2.setNAMA(NAMA);
        mahasiswa2.setNIM(NIM);
        session.save(mahasiswa2);
        session.getTransaction().commit();
    }
    
    public void updateMahasiswa2(String ID, String newNIM, String newNAMA){
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            
            Mahasiswa2 mahasiswa2 = (Mahasiswa2) session.get(Mahasiswa2.class, Integer.parseInt(ID));
            mahasiswa2.setNIM(newNIM);
            mahasiswa2.setNAMA(newNAMA);
            session.update(mahasiswa2);
            
            session.getTransaction().commit();
            
        }catch(Exception e){
            session.getTransaction().rollback();
            e.printStackTrace();
        }
    }
    
    public void updateMahasiswa(String oldID, String newID, String newNAMA){
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            
            
//            System.out.println(">> cek here: "+mahasiswa.getID().replace(",", "-"));
            
//            String[] idSplit = mahasiswa.getID().split(", ");
//            System.out.println("id lama: "+idSplit[0]);
//            
//            Query query = session.createQuery("from Mahasiswa where ID = :id");
//            query.setParameter("id", idSplit[0]);
//            
//            Mahasiswa oldMhs = getMhsById(idSplit[0]);
//            System.out.println("id old mhs: "+oldMhs.getID());
            
//            Mahasiswa m = (Mahasiswa) session.load(Mahasiswa.class, idSplit[0]);
//            m.setID(idSplit[1]);
//            m.setNAMA(mahasiswa.getNAMA());
//            System.out.println("id new mhs: "+idSplit[1]);
//            mahasiswa.setID(idSplit[1]);
//            mahasiswa.setNAMA(mahasiswa.getNAMA());
            
            Mahasiswa mahasiswa = getMhsById(oldID);
            mahasiswa.setID(newID);
            mahasiswa.setNAMA(newNAMA);
            session.update(mahasiswa);
            
//            String sql = "update Mahasiswa set ID = :newID, NAMA = :newNAMA where ID = :oldID";
//            SQLQuery query = (SQLQuery) session.createQuery(sql);
//            query.setParameter("newID", newID);
//            query.setParameter("oldID", oldID);
//            query.setParameter("newNAMA", newNAMA);
//            query.executeUpdate();
            
            session.getTransaction().commit();
            
//            System.out.println(">>TEST: "+mahasiswa.getID()+" | "+mahasiswa.getNAMA());
            
//            session.close();
        } catch (Exception e) {
            session.getTransaction().rollback();
            e.printStackTrace();
        }
    }
    
    public Mahasiswa getMhsById(String ID) {
        Mahasiswa mahasiswa = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            mahasiswa = (Mahasiswa) session.get(Mahasiswa.class, ID);
        } catch(Exception e) {
            e.printStackTrace();
        }
            
        session.close();
        return mahasiswa;
    }
    
    public Mahasiswa2 getMhsById2(String ID) {
        Mahasiswa2 mahasiswa2 = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            mahasiswa2 = (Mahasiswa2) session.get(Mahasiswa2.class, Integer.parseInt(ID));
        } catch(Exception e) {
            e.printStackTrace();
        }
            
        session.close();
        return mahasiswa2;
    }
    
    public void deleteMahasiswa(String ID){
         Session session = HibernateUtil.getSessionFactory().openSession();
         session.beginTransaction();
         Mahasiswa mahasiswa = (Mahasiswa) session.get(Mahasiswa.class, ID);
         
         if(mahasiswa != null){
             session.delete(mahasiswa);
             session.getTransaction().commit();
         }
    }
    
    public List<Mahasiswa> listMahasiswa(){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query query = session.createQuery("from Mahasiswa");
        List<Mahasiswa> mhsList = query.list();
        session.close();
        return mhsList;
    }
    
    public List<Mahasiswa2> listMahasiswa2(){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query query = session.createQuery("from Mahasiswa2");
        List<Mahasiswa2> mhsList2 = query.list();
        session.close();
        return mhsList2;
    }
    
    public boolean find(String name, String password) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        String SQL_QUERY = "from User u where u.username='" + name + "' and u.password='" + password + "'";
        System.out.println(SQL_QUERY);
        Query query = session.createQuery(SQL_QUERY);
        Iterator<User> it = query.iterate();
        List<User> list = query.list();
        if (list.size() > 0) {
            session.close();
            return true;
        }
        session.close();
        return false;
    }
    
    public void ngeTestRead() throws Exception {
        SessionFactory sessFact = HibernateUtil.getSessionFactory();
        Session sess = sessFact.getCurrentSession();
        Transaction tr = sess.beginTransaction();
        Query query = sess.createQuery("from User");
        List result = query.list(); 
        Iterator it = result.iterator();
        System.out.println("id  sname  sroll  scourse");
        while(it.hasNext()){
          User st = (User) it.next();
          System.out.print(st.getId_user());
          System.out.print("   "+st.getUsername());
          System.out.print("   "+st.getPassword());
          System.out.println();
        }
        sessFact.close();
    }
}
