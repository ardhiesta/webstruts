<%-- 
    Document   : message
    Created on : Oct 28, 2014, 6:05:19 AM
    Author     : linuxluv
--%>

<%@ taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><s:property value="message.message" /></title>
    </head>
    <body>
        <h1><s:property value="message.message" /></h1>
    </body>
</html>
